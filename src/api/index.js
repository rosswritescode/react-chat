const serverUrl = "ws://" + window.location.hostname + ":8080";
let websocketConnection = {};

// Singleton statuses
let initialized = false;
let connected = false;
let dispatch = false;

// Store events added before websocket is opened.
let events = [];

class chatAPI {
  constructor() {
    // Initialize once
    if(!initialized) {
      this.initialize();
      initialized = true;
    }
  }

  initialize() {
    websocketConnection = new WebSocket(serverUrl);
    websocketConnection.onopen = this.connected;
    websocketConnection.onmessage = this.detectEvent;
  }

  connected = () => {
    connected = true;
    // When connected, loop through any events and do them
    events.map(eventData => websocketConnection.send(JSON.stringify(eventData)));
  }

  // Send dispatch an action when sent down websocket
  detectEvent = (event) => {
    let jsonEvent = JSON.parse(event.data);    
    dispatch(jsonEvent);
  }

  // Set the dispatch method from redux
  connectDispatch(reduxDispatch) {
    dispatch = reduxDispatch;
  }

  // Send an action to the websocket server
  sendEvent(eventData) {
    if(connected) {
      websocketConnection.send(JSON.stringify(eventData));
    } else {
      events.push(eventData);
    }
  }
}

export default new chatAPI();