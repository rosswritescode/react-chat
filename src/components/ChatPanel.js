// React imports
import React, { Component } from 'react';

import classNames from 'classnames';

/* Import CSS */
import './ChatPanel.css';

class ChatPanel extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      message: ''
    }
  }

  // React events
  componentWillReceiveProps(newProps) {
    // Clear message if we're changing our selected user.
    if(newProps.selecteduser !== this.props.selecteduser) {
      this.clearMessage();
    }
  }
  
  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  // Component events
  scrollToBottom = () => {
    this.messagesScroller.scrollTop = this.messagesHolder.offsetHeight;
  }
  
  messageChange = (event) => {
    const message = event.target.value;
    this.setState({
      message
    });
  }

  sendMessage = () => {
    this.props.sendMessage(this.props.selecteduser.username, this.state.message);
    this.clearMessage();
  }

  clearMessage = () => {
    this.setState({
      message: ''
    })
  }

  // Render helper
  createChatBubbles() {
    const chatBubbles = [];
    let i = 0;

    // eslint-disable-next-line
    this.props.chatdata.map(chat => {
      
      const chatBubbleRowClass = classNames({
        'bubble-row': true,
        'bubble-row--them': !chat.yourMessage,
        'bubble-row--you': chat.yourMessage
      });

      chatBubbles.push(<div key={i++} className={chatBubbleRowClass}><div className="bubble">{chat.message}</div></div>)
    });

    return chatBubbles;
  }

  render() {
    return (
      <div className="chat-panel">
        <div className="message-panel" ref={(el) => { this.messagesScroller = el; }}>
          <div className="chat-bubbles" ref={(el) => { this.messagesHolder = el; }}>
            { this.createChatBubbles() }
          </div>
        </div>

        <div className="chat-tools pure-form pure-form-stacked">
          <textarea
            disabled={this.props.disabled}
            onChange={this.messageChange}
            value={this.state.message}></textarea>

          <button
            className="pure-button pure-button-primary"
            onClick={this.sendMessage}
            disabled={this.props.disabled || this.state.message === ''}>
            Send message
          </button>
        </div>
      </div>
    );
  }
}

export default ChatPanel;
