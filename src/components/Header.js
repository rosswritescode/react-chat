import React, { Component } from 'react';

/* Import CSS */
import './Header.css';

class Header extends Component {
  render() {
    return (
        <header>
          <h1>Chat!</h1>
          { !this.props.username || <div className="header__welcome">Welcome, {this.props.username}</div> }
        </header>
    );
  }
}

export default Header;
