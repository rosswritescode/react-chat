// Import react
import React, { Component } from 'react';
import classNames from 'classnames';

/* Import CSS */
import './UserList.css';

class UserList extends Component {
  clickUser = (event) => {
    const username = event.target.value;
    this.props.clickuser(username);
  }

  // Render helper
  createUserList() {
    const users = [];
    
    // eslint-disable-next-line
    this.props.userlist.map(user => {

      if(user.username === this.props.username) {
        return false;
      }
      
      // ADd class depending on status of user
      const usersClass = classNames({
        'user': true,
        'user--you': user.username === this.props.username,
        'user--loggedoff': !user.loggedin,
        'user--loggedon': user.loggedin,
        'user--selected': this.props.selecteduser && this.props.selecteduser.username === user.username
      });
  
       users.push(<button
         key={user.username}
         value={user.username}
         onClick={this.clickUser}
         className={usersClass}>
           {user.username}
       </button>)
    });

    return users;
  }

  render() {
    return (
      <div className="user-list">
      
        { this.createUserList() }

      </div>
    );
  }
}

export default UserList;
