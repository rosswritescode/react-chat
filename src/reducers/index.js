// Import constants
import {
  SET_USER_LIST,
  USER_LOGGED_IN,
  SEND_CHAT_MESSAGE,
  SELECT_USER,
  RECEIVE_CHAT_MESSAGE
} from '../constants/ActionTypes';

// Set init state
const initialState = {
  username: false,
  selecteduser: false,
  userlist: [],
  messages: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_LIST:
      const { userlist } = action;
      return {
        ...state,
        userlist: userlist
      };

    case USER_LOGGED_IN:
      const { username } = action;
      return {
        ...state,
        username
      };

    case SELECT_USER:
      const { selecteduser } = action;
      return {
        ...state,
        selecteduser
      };

    case RECEIVE_CHAT_MESSAGE:
      const { sender, sentmessage } = action;

      let newmessages = JSON.parse(JSON.stringify(state.messages));
      let receivedMessage = {
        yourMessage: false,
        from: sender,
        message: sentmessage
      }

      if(!newmessages[sender]) {
        newmessages[sender] = [];
      }
      newmessages[sender].push(receivedMessage);
      
      return {
        ...state,
        messages: newmessages
      };

    case SEND_CHAT_MESSAGE:
      const { recipient, message } = action;

      let messages = JSON.parse(JSON.stringify(state.messages));
      let newMessage = {
        yourMessage: true,
        message: message
      }

      if(!messages[recipient]) {
        messages[recipient] = [];
      }
      messages[recipient].push(newMessage);
      
      return {
        ...state,
        messages
      };

    default:
      return state;
  }
};

/*
  Exports
*/
export default reducer;

export function getChatData(state, username) {
  if(!state.messages[username]) {
    state.messages[username] = [];
  }

  return state.messages[username];
}

export function getSelectedUser(state, selecteduser) {
  return state.userlist.find(user => user.username === selecteduser);
}