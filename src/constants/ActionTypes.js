export const GET_USER_LIST = 'GET_USER_LIST';
export const SET_USER_LIST = 'SET_USER_LIST';
export const USER_LOG_IN = 'USER_LOG_IN';
export const USER_LOGGED_IN = 'USER_LOGGED_IN';
export const SEND_CHAT_MESSAGE = 'SEND_CHAT_MESSAGE';
export const RECEIVE_CHAT_MESSAGE = 'RECEIVE_CHAT_MESSAGE';
export const ERROR = 'ERROR';
export const SELECT_USER = 'SELECT_USER';