import React from 'react'
import { connect } from 'react-redux'

import { sendMessage, selectUser } from '../actions'
import { getChatData, getSelectedUser } from '../reducers'

// Import component
import Chat from '../screens/Chat'

const ChatContainer = ({ userlist, username, selecteduser, chatdata, sendChatMessage, selectUser }) => (
  <Chat
    userlist={ userlist }
    username={ username }
    selecteduser={ selecteduser }
    sendChatMessage={ sendChatMessage }
    selectUser={ selectUser }
    chatdata={ chatdata } />
)

const mapStateToProps = (state) => {
  return ({
    userlist: state.userlist,
    username: state.username,
    selecteduser: getSelectedUser(state, state.selecteduser),
    chatdata: getChatData(state, state.selecteduser)
  })
}

const mapDispatchToProps  = (dispatch, ownProps) => ({
  sendChatMessage: (recipient, message) => dispatch(sendMessage(recipient, message)),
  selectUser: (username) => dispatch(selectUser(username))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatContainer)
