import React from 'react'
import { connect } from 'react-redux'

import { userLogin } from '../actions'

// Import component
import Login from '../screens/Login'

const LoginContainer = ({ userlist, onLoginClicked }) => (
  <Login
  userlist={ userlist }
    onLoginClicked={onLoginClicked} />
)

const mapStateToProps = (state) => {
  return ({
    userlist: state.userlist
  })
}

const mapDispatchToProps  = (dispatch, ownProps) => ({
  onLoginClicked: username => dispatch(userLogin(username))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer)
