// React objects
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import registerServiceWorker from './registerServiceWorker';

// Our reducer
import rootReducer from './reducers'

// Our API singleton
import API from './api';

// App container
import App from './App';

// Our store
const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() // For redux dev tools
);

// Give dispatch event to our API to process server requests
API.connectDispatch(store.dispatch)

// Render our app
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

registerServiceWorker();