// React objects
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { hot } from 'react-hot-loader'

// Import components
import Header from './components/Header';

// Our containers
import Login from './containers/Login';
import Chat from './containers/Chat';

// Get user list action
import { getUserList } from './actions';

// CSS
import './App.css';

class App extends Component {

  // Load user ist when component mounts
  componentWillMount() {
    this.props.getUserList();
  }

  render() {
    return (
      <div className="App">

        <Header username={this.props.username}/>

        { !this.props.username ? <Login /> : <Chat /> }

      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return ({
    username: state.username
  })
}

const mapDispatchToProps  = (dispatch, ownProps) => ({
  getUserList: () => dispatch(getUserList())
});

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default hot(module)(AppContainer);
