// Import constants
import {
  SET_USER_LIST,
  GET_USER_LIST,
  USER_LOG_IN,
  SEND_CHAT_MESSAGE,
  SELECT_USER
} from '../constants/ActionTypes';

// Our API singleton
import API from '../api';

/*
  Actions
*/
export function setUserList(users) {
  return {
    type: SET_USER_LIST,
    users
  }
}

export function selectUser(selecteduser) {
  return {
    type: SELECT_USER,
    selecteduser
  }
}

export function getUserList(users) {
  const action = {
    type: GET_USER_LIST
  }

  API.sendEvent(action);

  return action;
}

export function userLogin(username) {
  const action = {
    type: USER_LOG_IN,
    username
  }
  
  API.sendEvent(action);
  
  return action;
}

export function sendMessage(recipient, message) {
  const action = {
    type: SEND_CHAT_MESSAGE,
    recipient,
    message
  };
  
  API.sendEvent(action);

  return action;
}
