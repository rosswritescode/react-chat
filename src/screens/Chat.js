import React, { Component } from 'react';

// import components
import UserList from '../components/UserList';
import ChatPanel from '../components/ChatPanel';

/* Import CSS */
import './Chat.css';

/*
  Chat screen: Main screen seen after log in
*/
class Chat extends Component {
  
  render() {
    return (
        <div className="chat-screen">

          <UserList
            userlist={this.props.userlist}
            username={this.props.username}
            clickuser={this.props.selectUser}
            selecteduser={this.props.selecteduser} />

          { !!this.props.selecteduser || <div className="empty-message">Select a user to chat to from the side bar.</div> }

          { !this.props.selecteduser || <ChatPanel 
            selecteduser={this.props.selecteduser}
            chatdata={this.props.chatdata}
            sendMessage={this.props.sendChatMessage}
            disabled={!this.props.selecteduser || !this.props.selecteduser.loggedin} /> }

        </div>
    );
  }
}

export default Chat;
