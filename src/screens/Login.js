import React, { Component } from 'react';

/* Import CSS */
import './Login.css';


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedUser: false
    }
  }

  // Component events
  changeUser = (el) => {
    this.setState({
      selectedUser: el.target.value
    });
  }

  logIn = () => {
    let username = this.state.selectedUser;
    this.props.onLoginClicked(username);
  }

  // Render helper
  createUsersDropdown() {
    const userOptions = [];
    userOptions.push(<option key="default" value=''>...</option>)
    
    // eslint-disable-next-line
    this.props.userlist.map(user => {
      // Only add to dropdown if the user is not currently logged in.
      if(!user.loggedin) {
        userOptions.push(<option key={user.username}>{user.username}</option>)
      }
    });

    return (<select onChange={this.changeUser}> { userOptions } </select>);
  }

  render() {
    return (
        <div className="login pure-form pure-form-stacked">
          <h2>Log in as:</h2>

          { this.createUsersDropdown() }

          <button
            className="pure-button pure-button-primary"
            disabled={!this.state.selectedUser}
            onClick={this.logIn}>
            Continue
          </button>
        </div>
    );
  }
}

export default Login;
