This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

# Introduction
This is a simple chat app built with React. You select which user you want to log in as, then you can send messages to any other user that is logged in at the time.

# Running the project

1. Clone repo, go to folder, run ```npm install```

2. Run ```npm start```. This will start a websockets server, and run the React app.

3. If it does not open automatically, go to `http://localhost:3000/`

# Using the chat app

Once the servers have started and you have the app opened in a browser, select which user you'd like to log in as and press `Continue`.

Do the same again but on a different browser (or private browsing mode), so you have someone to talk to! There are up to 4 different users available.

Users that are not logged in will show as greyed-out on the users selct sidebar. Users that are available will have black text.

Choose one of the logged in users, and start messaging.

They will see your messages if they click into the chat they have with you.

# Future development

This is a very basic app created for demo purposes. There is much that can be done to make it more featureful, not limited to:

* Using Sass/Less for styling
* Updating styling
* Animate UI
* Store users/chats in database
* Show new message notifications
* Choose username/password to log in
* Send offline messages

# Contact

mail@rosswritescode.com