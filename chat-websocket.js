// Start websocket stuff
const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 8080 });

// Our dummy user data (move to mongodb, create users properly etc.)
const usersLoggedOn = {
  'Adam': false,
  'Betty': false,
  'Chris': false,
  'Daria': false
};

// Lookup username by ws object
const usernameLookup = new WeakMap();


/*
 Create actions to send to react
*/
function getUserList() {
  let activeUsers = [];

  for (const [key, value] of Object.entries(usersLoggedOn)) {
    activeUsers.push({
      username: key,
      loggedin: !!value
    });
  }

  return activeUsers;
}

function getUserLogInEvent(username) {
  return {
    type: 'USER_LOGGED_IN',
    username: username
  }
}
function getChatEvent(sender, message) {
  return {
    type: 'RECEIVE_CHAT_MESSAGE',
    sender: sender,
    sentmessage: message
  }
}

function getSetUserListEvent() {
  return {
    type: 'SET_USER_LIST',
    userlist: getUserList()
  }
}

// Broadcast
wss.broadcast = function broadcast(data) {
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data);
    }
  });
};

// Handle connection
wss.on('connection', function connection(ws) {
  console.log("Connection");
  
  ws.on('message', function incoming(datastring) {
    let data  = JSON.parse(datastring);

    // switch statement, split out
    switch(data.type) {
      case 'GET_USER_LIST':
        ws.send(JSON.stringify(getSetUserListEvent()));
      break;

      case 'USER_LOG_IN':
        logOut(ws); // Check we're not hoarding a username

        // Register user, tell other users
        let username = data.username;
        usersLoggedOn[username] = ws;
        usernameLookup.set(ws, username);

        ws.send(JSON.stringify(getUserLogInEvent(username))); // Confirm log in
        wss.broadcast(JSON.stringify(getSetUserListEvent()));  // Send updated user list to all
      break;
      case 'SEND_CHAT_MESSAGE':
        // Send chat to 
        let sendMessageTo = usersLoggedOn[data.recipient];
        let messageSender = usernameLookup.get(ws);

        if(sendMessageTo && messageSender) {
          sendMessageTo.send(JSON.stringify(getChatEvent(messageSender, data.message)));
        } else {
          ws.send(JSON.stringify(errorMessage));
        }
      break;
      default:
        // Send error message
        ws.send(JSON.stringify(errorMessage));
      break;
    }
  });

  // Remove user from logged on list.
  ws.on('close', data => logOut(ws));
});

// Log out user
function logOut(ws) {
  let username = usernameLookup.get(ws);
  if(username) {
    usersLoggedOn[username] = false;
    wss.broadcast(JSON.stringify(getSetUserListEvent()));
  }
}